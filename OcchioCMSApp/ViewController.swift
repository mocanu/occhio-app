//
//  ViewController.swift
//  OcchioCMSApp
//
//  Created by Razvan Mocanu on 01.03.18.
//  Copyright © 2018 Occhio. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let url = URL(string: "http://thehighrise.occhio.de")
        //let url = URL(string: "https://occhio-testing.webpipeline.net")
        let url = URL(string: "https://occhio-app.firebaseapp.com")
        let request = URLRequest(url: url!)
        webView.load(request)
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

